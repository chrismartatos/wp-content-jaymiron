<?php get_header(); ?>

<div id="content" class="subpage subpage-layout">
	<?php if( have_posts() ): the_post(); ?>

		<div class="no-banner cf"></div>

		<header id="subpage-header" class="header">
			<h1><?php the_title(); ?></h1>
			<div class="divider mauto"></div>
		</header>


		<article class="cf">
			<div class="container">
				<div class="row">
					<div class="col-lg-1 tc"></div>
					<div class="col-lg-10 tc"><?php the_content(); ?></div>
					<div class="col-lg-1 tc"></div>
				</div><!--END .row-->
			</div><!--END .container-->


			<nav id="project-nav" class="container">
				<?php if(get_field('project_dimensions')): ?>
				<div class="item">
					<cite class="item-1" data-title="Dimensions">
						<div class="vcenter-outer">
							<div class="vcenter-inner">
							<h5>Dimensions</h5>
							<span class="divider mauto"></span>
							<p><?php the_field('project_dimensions'); ?></p>
							</div>
						</div>
					</cite>
				</div>
				<?php endif; ?>
				<?php if(get_field('project_materials')): ?>
				<div class="item">
					<cite class="item-2" data-title="Materials">
						<div class="vcenter-outer">
							<div class="vcenter-inner">
							<h5>Materials</h5>
							<span class="divider mauto"></span>
							<p><?php the_field('project_materials'); ?></p>
							</div>
						</div>
					</cite>
				</div>
				<?php endif; ?>
				<?php if(get_field('wood_finish')): ?>
				<div class="item">
					<cite class="item-3" data-title="Wood Finish">
						<div class="vcenter-outer">
							<div class="vcenter-inner">
							<h5>Wood Finish</h5>
							<span class="divider mauto"></span>
							<p><?php the_field('wood_finish'); ?></p>
							</div>
						</div>
					</cite>
				</div>
				<?php endif; ?>
			</nav>


			<div class="entry-content">
				<div class="container">
					<div class="row">
					<?php include('elements/flexible-content-subpage.php'); ?>
					</div>
				</div>
			</div>

		</article>

	<?php else: ?>
		<p>Sorry, this page not longer exists.</p>
	<?php endif; ?>

	<?php
	$imgsrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full");
	$get_featimg = $imgsrc[0];

	?>
	<div class="cf half-height bg-cover bg-fixed featured-fixed" <?php echo 'style="background-image:url('.$get_featimg.')"';?>></div>

	<nav id="work-nav" class="cf">

		<?php
		//Previous
    $nextPost = (get_next_post())? get_next_post() : get_post();

		if($nextPost):
			$nextTitle = get_the_title($nextPost->ID);
			$nextLink = get_permalink($nextPost->ID);
	    $nextThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($nextPost->ID,'thumbnail'));
			?>
			<a class="prev arrows" rel="prev" href="<?php echo $nextLink; ?>" title="<?php echo $nextTitle; ?>" title="<?php echo $nextTitle; ?>">
					<span class="ent-arr">&larr;</span>
					<div class="thumb bg-cover" style="background-image:url(<?php echo $nextThumbnail[0]; ?>);"></div>
			</a>
		<?php
		endif;
		?>

		<a class="all" href="<?php the_permalink(21); ?>">
			<div class="wrap cf">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</a>

		<?php
		//Next
		$prevPost = (get_previous_post())? get_previous_post() : get_post();

		if($prevPost):
			$prevTitle = get_the_title($prevPost->ID);
			$prevLink = get_permalink($prevPost->ID);
	  	$prevThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($prevPost->ID,'thumbnail'));
			?>
			<a class="next arrows" rel="next" href="<?php echo $prevLink; ?>" title="<?php echo $prevTitle; ?>" data-title="<?php echo $prevTitle; ?>">
	        <span class="ent-arr">&rarr;</span>
	        <div class="thumb bg-cover" style="background-image:url(<?php echo $prevThumbnail[0]; ?>);"></div>
	    </a>
			<?php
		endif;
		?>
	</nav>
</div><!--END #content-->

<?php get_footer(); ?>
