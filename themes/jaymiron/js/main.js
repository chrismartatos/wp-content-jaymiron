/*
	**Jaymiron.com**	
*/
(function($) 
{
  'use strict';
  
  /* Start jaymiron */
  var jaymiron = window.jaymiron || {};
  
  /* FN: onload basics 
  ----------------------------------------------------------------------------*/
  jaymiron.onLoadBasics = function()
  {
  	 var events = 
  	 {
  		KillInternetExplorer: function()
  		{
	  		//Define Internet explorer version compatibility +
	  		var IE_compatible = 11;
	  		
	  		//Check browser
	  		var ie_check = navigator.userAgent.toLowerCase();
	  
			if(ie_check.indexOf('msie') != -1)
			{
				var IE_version = parseInt(ie_check.split('msie')[1]); 
		      		
				if(IE_version < IE_compatible) 
				{
					die_IE().trigger(IE_version,IE_compatible);
				}
			}
		},
  		shortcodes: function()
  		{
	  		scroll_to();
	  		full_height();
	  		//half_height();
	  		//parallax_init();
	  		visibleElement();
	  		home_banner();
	  		scrollDownHeader();
	  		lazyLoad();
	  		onTouch();
	  		$("#hide-email").hideEmail();
  		},
  		mainNav: function()
  		{
	  		var targetMain = $("#main-nav ul > li"),
	  			targetMobile = $("#mobile-nav-container"),
	  			Window = $(window),
	  			hamburger = $("#hamburger"),
	  			close = $("#close-nav"),
	  			pos = Window.scrollTop(), 
	  			nav = $("#main-nav"),
	  			header = $("#header"),
	  			$body = $("body"),
	  			gutter = 200,
	  			nav_pos = gutter;    
	  			
			    /* SCROLL FN */
			    Window.scroll(function() 
			    {
			      pos = Window.scrollTop(); 
			      
			      (pos>=nav_pos)?header.addClass("scroll-init"):header.removeClass("scroll-init"); 
			    });
			    
			    //Main nav - hover
	  			targetMain.hover(function()
	  			{	  				
	  				targetMain.find("a").removeClass("hover");
	  				
	  				$("a", this).addClass("hover");
	  			});
	  			
	  			//Mobile
				hamburger.click(function(e)
				{
					e.preventDefault();
					
					$(this).toggleClass("is-clicked"); 
					$body.toggleClass("opened-menu");
					
					//in firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
					if( targetMobile.hasClass("active") ) {
						targetMobile.removeClass("active").one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",function()
						{
							$body.removeClass("overflowHidden");
						});
					} 
					else 
					{
						targetMobile.addClass("active").one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",function()
						{
							$body.addClass("overflowHidden");
						});	
					}
					
					return false;
				});
				
				//Close fn
				close.click(function(e)
				{
					e.preventDefault();
					
					hamburger.removeClass("is-clicked"); 
					$body.removeClass("opened-menu");
					targetMobile.removeClass("active").one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",function()
					{
						$body.removeClass("overflowHidden");
					});	
					
					return false;
				});
				
  		},
  		sitePreloader: function()
  		{
  			var $body = $('body'),
  				$window = $(window),
  				$preloader = $("#preloader"),
  				$page = $("#page");
  			  			
		    $body.addClass("loading");
		     
		    //Show preloader with svg animation to first time visitors
		    if(sessionStorage.getItem('JayMironWoodworking') !== '540') 
		    {  
			    //Start session - first session
			    sessionStorage.setItem('JayMironWoodworking', '540');
			    			   	
			   	$body.addClass("svg-loading");
				
				
				setTimeout(function()
				{
					$preloader.find(".wrap-loader").addClass("end");
				},1700);
				
				
				setTimeout(function()
				{
					$("body").removeClass("loading").addClass("loaded");
				},3900);
				
				//Removed because of a bug
				//$preloader.find("#jaymiron").on('webkitAnimationEnd oanimationend msAnimationEnd animationend',function(e){});
  				
			}
			else 
			{
				//Display pace loader to visitors
				preloaderFN();
			}
		}	
 
  	};
  	
  	return events;
  };
  

  /* DOM Ready || Trigger functions 
  ===============================================================================*/
  jQuery(document).ready(function()
  {
  	 //Onload events and functions
	 jaymiron.onLoadBasics().KillInternetExplorer();
	 jaymiron.onLoadBasics().sitePreloader();
	 jaymiron.onLoadBasics().shortcodes();
	 jaymiron.onLoadBasics().mainNav(); 
	 
	 
  });/*Close DOM ready*/
  

/* Shortcodes: On Touch
--------------------------------------------------------------------------------------------------------*/
var onTouch = (function()
{
	var $touch = $("#page .touch-event");
	
	if($touch.length)
	{
		$touch.on("touchstart",function()
		{
		   $(this).toggleClass("touch");
		   
		});
		
		$touch.on("touchend",function()
		{
		   $(this).toggleClass("touch");
		});
	}
	
});



/* Shortcodes: preloader - window fully load fn
--------------------------------------------------------------------------------------------------------*/
var preloaderFN = (function()
{
	$(window).load(function()
	{ 
  		$("body").removeClass("loading").addClass("loaded");  	  		
  	});
});


/* home
--------------------------------------------------------------------------------------------------------*/
var home_banner = (function()
{
	 var $banner = $("#hero > .main-image");
	 
	 if($banner.length)
	 {
		 setInterval(function()
		 {
			//Change opacity with css
		    $banner.toggleClass("change-opacity");
		}, 1900);
	 }
});


/* Shortcodes: parallax_init
--------------------------------------------------------------------------------------------------------*/
var parallax_init = (function()
{
	var $window = $(window).width();
	
	if($window > 1040)
	{
		var $scrollr = skrollr.init();
	}
});



/* Lazy load
--------------------------------------------------------------------------------------------------------*/
var lazyLoad = (function()
{
	var $element = $("#page").find(".lazy-load-init");
	
	//alert($("#page").find(".lazy-load-init").length)
	
	if($element.length)
	{
		$element.show().lazyload({
			effect: "fadeIn"
		});	
	}
});



/* Shortcodes: full height
--------------------------------------------------------------------------------------------------------*/
var full_height = (function()
{
	if($(".full-height").length)
	{
		var target = $("#page .full-height"),
			getWindow = $(window);
			
			target.css("height", getWindow.height());
			
			getWindow.resize(function()
			{
				target.css("height", getWindow.height());
			});
	}	
});


/* Shortcodes: half height
--------------------------------------------------------------------------------------------------------*/
var half_height = (function()
{
	if($(".half-height").length)
	{
		var target = $("#page .half-height"),
			getWindow = $(window);
			
			target.css("height", getWindow.height()/1.3);
			
			getWindow.resize(function()
			{
				target.css("height", getWindow.height()/1.3);
			});
	}	
});


/* Scroll down header
--------------------------------------------------------------------------------------------------------*/
var scrollDownHeader = (function($this)
{
	
	if($(window).width() < 1900)
	{
		// Hide Header on on scroll down
		var didScroll;
		var lastScrollTop = 0;
		var delta = 5;
		var $header = $("#page");
		var navbarHeight = $("#header").outerHeight();
		
		//Scroll fn
		$(window).scroll(function(event)
		{
		    didScroll = true;
		});
		
		setInterval(function() 
		{
		    if(didScroll) 
		    {
		        var st = $(this).scrollTop();
		    
			    // Make sure they scroll more than delta
			    if(Math.abs(lastScrollTop - st) <= delta)
			        return;
			    
			    // If they scrolled down and are past the navbar, add class .nav-up.
			    // This is necessary so you never see what is "behind" the navbar.
			    if (st > lastScrollTop && st > navbarHeight)
			    {
			        // Scroll Down
			         $header.addClass("scrolling-down");
			    } else {
			        // Scroll Up
			        if(st + $(window).height() < $(document).height()) 
			        {
			          $header.removeClass("scrolling-down");
			        }
			    }
			    
			    lastScrollTop = st;
			    
		        didScroll = false;
		    }
		}, 250);
	}
});

/* Visible element: css animation
--------------------------------------------------------------------------------------------------------*/
var visibleElement = (function()
{
	var $element = $("#content").find(".visible-animation"),
		$window = $(window).width();
	
	if($window > 767 && $element.length)
	{
		$(window).scroll(function(event) 
		{  
		  $element.each(function(i, el) 
		  {
		    var el = $(el);
		    if (el.visible(true)) 
		    {
		      el.addClass("css-animation"); 
		    } 
		  });
		  
		});	
	}
	

});

/* Visible element FN: css animation
--------------------------------------------------------------------------------------------------------*/
$.fn.visible = function(partial) 
{
  var $t            = $(this),
      $w            = $(window),
      viewTop       = $w.scrollTop(),
      viewBottom    = viewTop + $w.height(),
      _top          = $t.offset().top,
      _bottom       = _top + $t.height(),
      compareTop    = partial === true ? _bottom : _top,
      compareBottom = partial === true ? _top : _bottom;

return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

};


/* Shortcodes: scroll to
--------------------------------------------------------------------------------------------------------*/
var scroll_to = (function()
{
	if($("#page .scroll-to").length)
	{
		$('.scroll-to:not(.scroll-to-on)').each(function()
		{
			$(this).click(function(e)
			{
				e.preventDefault();
	
				var $id = $(this).attr('href'),
				    offset = 0,
				    speed = 800,
				    callback;
	
				// speed
				if($(this).data('speed'))
					speed = parseInt($(this).data('speed'));
	
				// offset
				if($(this).data('offset'))
					offset = parseInt($(this).data('offset'));
	
				// easing
				if($(this).data('easing'))
					easing = parseInt($(this).data('easing'));
	
				var top = $($id).offset().top - offset;
	
				// animation
				if(!$('html:animated, body:animated').length)
					$('html,body').animate({scrollTop: top}, speed, function()
					{
						typeof callback === 'function' && callback();
					});
	
			});
	
			$(this).addClass('scroll-to-on');
			
			return false;
		});	
	}
});



/* Shortcodes: Hide email
--------------------------------------------------------------------------------------------------------*/
//Declare function in jQuery scope
$.fn.hideEmail = function()
{
  var $target = $(this);

  if($target.length)
  {
    //vars
    var $email = $target.attr("data-hide"),
        _epattern = /([A-Za-z0-9.-]+.[A-Za-z])@([a-z]+)/,
        _emailFinal= $email.replace(_epattern,"$2@$1");

    //$link.attr("href","mailto:"+_emailFinal);
    $target.text(_emailFinal);
  }
};


/* Shortcodes: die IE
--------------------------------------------------------------------------------------------------------*/
var die_IE = (function()
{	
	var event = {
		trigger: function(IE_version,IE_compatible)
		{
			var version = IE_version,
				IE_title = '<h5>Your browser is out-of-date</h5>',
		        IE_link = '<a rel="nofollow" class="btn-square black" target="_blank" href="https://browser-update.org/update.html"><span>Update your Browser</span></a>',
		        IE_desc = '<div class="desc">We apologize for showing this message but your browser is TOO OLD. If you want to view this website please click the following button and update your browser with the latest version of Internet Explorer, Firefox, Chrome etc. Thank you!</div>',
		        fuck_off_html = '<div id="ie-message"><div class="vcenter-outer"><div class="vcenter-inner"><div class="wrap cf tc"><a onclick="javascript:shittyBrowser();" class="fa fa-times"></a>'+IE_title+IE_desc+IE_link+'</div></div></div></div>';
		    
		    //Append html
		    $("#footer").after(fuck_off_html);
		}
	};
	return event;
});



})(jQuery);



/* Shortcodes: browser onclick
-------------------------------*/
function shittyBrowser()
{
	$("#ie-message").hide();
	
	alert("Closing the pop up window won't help you...please update your browser."); 
		
	setTimeout(function()
	{ 
		$("body").css("opacity","0");
		
		alert("Unfortunately you need to update your browser or this will continue to happening. Please use Chrome, Firefox or Opera."); 
	},2000);
}