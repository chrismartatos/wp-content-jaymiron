$(function()
{
	var $grid = $('.media-wrapper');
	
    if($.isFunction($.fn.isotope))
    {
	  $(window).load(function()
	  { 
		  $grid.addClass('loaded');
		  
		  $grid.isotope({
	        itemSelector: '.media-item'
	      });
	  });
    } 
    else 
    {
	   $grid.addClass('loaded'); 
    }

});