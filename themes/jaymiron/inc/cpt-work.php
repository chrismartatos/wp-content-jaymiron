<?php
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}
/*-------------------------------------------------------------------------------------
     My Custom Post Type: https://codex.wordpress.org/Post_Types
-------------------------------------------------------------------------------------*/
add_action( 'init', 'work_custom_post_type');

function work_custom_post_type()
{
    //Vars
	  $singular = 'Project';
    $singular_lowercase = 'project';
	  $plural = 'Work';
	  $slug = 'jaymiron-furnitures';
	  $post_type = 'work';
	  $supports = array('title', 'editor', 'thumbnail', 'page-attributes', 'revisions');

    //Labels
	  $labels = array(
		'name' => _x( $plural, 'post type general name'),
		'singular_name' => _x( $singular, 'post type singular name'),
		'add_new' => _x('Add New', $singular_lowercase ),
		'add_new_item' => __('Add New '. $singular_lowercase),
		'edit_item' => __('Edit '. $singular_lowercase ),
		'new_item' => __('New '. $singular_lowercase ),
		'view_item' => __('View '. $singular_lowercase),
		'search_items' => __('Search '. $plural),
		'not_found' =>  __('No '. $singular .' found'),
		'not_found_in_trash' => __('No '. $singular .' found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => $plural
	  );

    //Args
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
    'menu_icon' => 'dashicons-grid-view',
		'rewrite' => Array('slug'=> $slug ),
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		'supports' => $supports
	  );

	  register_post_type( $post_type, $args );
}


/*-------------------------------------------------------------------------------------
     Taxonomy
-------------------------------------------------------------------------------------*/
add_action( 'init', 'work_custom_taxanomy' );

function work_custom_taxanomy()
{
	register_taxonomy(
		'category', /* Unique name */
		'work', /* Asign to post type */
		array(
			'label' => __( 'Choose Category' ),
			'rewrite' => array( 'slug' => 'work-category' ),
			'hierarchical' => true,
      'show_ui' => true,
      'query_var' => true,
      'show_admin_column' => true,
		)
	);
}
