<?php
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

/*----------------------------------------------------------
		ACF Options
----------------------------------------------------------*/
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme Settings',
		'menu_title' 	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-options',
		'capability' 	=> 'edit_posts',
		'parent_slug' 	=> '',
		'position'		=> false,
		'icon_url'		=> false,
		'redirect' 	=> false
	));
}



/*===========================================================================================================
    !IMPORTANT Hide Custom Fields area from Admin on Staging and Production
		Use acf locally to create new fields - acf-json folder active
============================================================================================================*/

/**
 * Hide ACF Admin on Staging/Production
 */

function awesome_acf_hide_acf_admin() {

    // get the current site url
    $site_url = get_bloginfo( 'url' );

    // an array of protected site urls
    $protected_urls = array(
    'https://www.jaymiron.com',
		'http://www.jaymiron.com'
    );

    // check if the current site url is in the protected urls array
    if ( in_array( $site_url, $protected_urls ) ) {

        // hide the acf menu item
        return false;

    } else {

        // show the acf menu item
        return true;

    }

}

add_filter('acf/settings/show_admin', 'awesome_acf_hide_acf_admin');
