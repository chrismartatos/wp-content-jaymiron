<?php
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

/*-------------------------------------------------------------------------------
	Title
-------------------------------------------------------------------------------*/
add_theme_support( 'title-tag' );


/*----------------------------------------------------------
		JS - footer - Register Scripts
----------------------------------------------------------*/
function front_end_scripts()
{
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js', array(), null, true );
    wp_register_script( 'front-end-plugins', get_template_directory_uri() . '/js/plugins.js', array( 'jquery' ), null, true );
    wp_register_script( 'front-end-script', get_template_directory_uri() . '/js/main.js', array( 'front-end-plugins' ), null, true );
    wp_enqueue_script( 'front-end-script' );
}

add_action( 'wp_enqueue_scripts', 'front_end_scripts' );



/*----------------------------------------------------------
		CSS - styles - header
----------------------------------------------------------*/
function front_end_styles()
{
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Droid+Serif:400,700|Roboto:400,300,700,500', false );
	wp_register_style( 'front-end-bootstrap', get_template_directory_uri() . '/css/reset.css', array(), null, 'all' );
    wp_register_style( 'front-end-styles', get_template_directory_uri() . '/css/styles.css', array(), null, 'all' );
    wp_register_style( 'front-end-plugins', get_template_directory_uri() . '/css/plugins.css', array(), null, 'all' );
    wp_register_style( 'front-end-media-queries', get_template_directory_uri() . '/css/media-queries.css', array(), null, 'all' );

    wp_enqueue_style( 'google-fonts' );
    wp_enqueue_style( 'front-end-bootstrap' );
    wp_enqueue_style( 'front-end-styles' );
    wp_enqueue_style( 'front-end-plugins' );
    wp_enqueue_style( 'front-end-media-queries' );
}

add_action( 'wp_enqueue_scripts', 'front_end_styles' );


/*----------------------------------------------------------
		Remove code from header
----------------------------------------------------------*/
add_action('widgets_init', 'my_remove_recent_comments_style');
function my_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}

remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
add_filter('xmlrpc_enabled', '__return_false');


/*-------------------------------------------------------------------------------
	Disable rest api
-------------------------------------------------------------------------------*/
add_filter('rest_enabled', '__return_false');
add_filter('rest_jsonp_enabled', '__return_false');

remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'template_redirect', 'rest_output_link_header', 11 );

remove_action( 'rest_api_init', 'wp_oembed_register_route' );
add_filter( 'embed_oembed_discover', '__return_false' );
remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
remove_action( 'wp_head', 'wp_oembed_add_host_js' );

/*----------------------------------------------------------
		Basics
----------------------------------------------------------*/
// Enable The Post Thumbnail
add_theme_support( 'post-thumbnails' );

/*----------------------------------------------------------
		Menus - Options
----------------------------------------------------------*/
register_nav_menus(array(
	'main_nav' => 'Main Menu',
	'mobile_nav' => 'Mobile Menu',
	'footer_nav' => 'Footer Menu'
));
