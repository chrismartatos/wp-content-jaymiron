<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/chrismartatos/admin/admin.css">

<section class="bbt-admin cf">
	<h1>Documentation</h1>
	
	<nav class="bbt-admin-nav hul cf">
		<ul>
			<li><a href="#general">General</a></li>
			<li><span class="divider">|</span></li>
			<li><a href="#layout">Layout Shortcodes</a></li>
			<li><span class="divider">|</span></li>
			<li><a href="#media">Media Shortcodes</a></li>
		</ul>
	</nav>
	
	<section class="bbt-admin-section" id="general">
		<h2>General</h2>
		<div class="wrap cf">
			<h3>Page Templates</h3>
			<p>There are serveral templates in this Theme. You should be using the Default Template for all sub pages. All other templates such as Homepage, Programs.</p>
			<p>Each Template has its own template options under text editor, please follow the properties instuction.</p>
		</div>
	</section>

</section>