<?php
/*
Template Name: Home - Template
*/
?>

<?php get_header(); ?>
<?php
$imgsrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "Full");
$embed = get_field('embed_video');

if(!empty($embed)):
?>
<div id="hero" class="cf hero-embed">
	<div class="videowrapper">
		<?= $embed; ?>
	</div>
</div>
<?php
else:
$getimg = $imgsrc[0];
?>
<div id="hero" class="cf">
	<div class="main-image bg-image bg-cover bg-fixed" <?php if(!empty($getimg)){ echo 'style="background-image:url('.$getimg.')"'; } ?>></div>
	<?php if( get_field('overlapped_image') ): ?>
		<div class="mask bg-image full-height bg-cover bg-fixed"<?php echo ' style="background-image:url('.get_field('overlapped_image').')"';?>></div>
	<?php endif; ?>
			<?php if( have_posts() ): the_post(); ?>

				<?php if($post->post_content != "") : ?>
					<div class="quote"><?php the_content(); ?></div>
				<?php endif; ?>

			<?php endif; ?>

		<a class="scroll-to wrap-scroll" href="#work-projects" data-offset="50">
			<h1 class="text"><?php the_field('keep_scrolling_text'); ?></h1>
			<span class="scroll-arrows one"></span>
			<span class="scroll-arrows two"></span>
			<span class="scroll-arrows three"></span>
		</a>
</div>
<?php endif; ?>

<div id="content">
	<div id="work-projects" class="cf">
			<div class="wrap-projects cf">
			<?php
			//Show all posts
			$args = array(
				'post_type' => 'work',
				'posts_per_page'=> '10',
				'order'=> 'DESC',
				'orderby' => 'date',
				'post_status' => 'publish'
			);

			//Define the loop based on arguments
			$query = new WP_Query( $args );

			//Display the projects
			while ( $query->have_posts() ) : $query->the_post();

			//Get image source
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			?>

			<?php get_template_part('elements/project-item'); ?>

			<?php
				endwhile;
				wp_reset_postdata();
			?>

			</div>
	</div>


	<!--About 2-->
	<div id="home-about-layout" class="cf visible-animation bg-cover" <?php if( get_field('add_background_image_about') ): ?><?php echo ' style="background-image:url('.get_field('add_background_image_about').');"'; ?><?php endif; ?>>
		<div class="main-wrapper cf">
		<?php
			$about_id = 19;
			$about_title = get_the_title($about_id);
			$about_href = get_permalink($about_id);
		?>
		<article class="wrap-info container">
			<div class="cf">
				<div class="col item-1">
					<div class="desc cf">
						<h3><?php if(get_field('about_title')) { echo get_field('about_title'); } ?></h3>
						<div class="divider center-mobile"></div>
						<?php the_field('about_description') ?>
						<a href="<?php echo $about_href; ?>" class="btn large black chainsaw-icon"><i>Read full bio</i><span class="chainsaw"></span></a>
					</div>
				</div>
				<?php

				$images = get_field('home_about_gallery');

				if( $images ): ?>
			        <?php foreach( $images as $image ): ?>
		                <figure class="col">
		                	<a class="bg-cover scale-img lazy-load<?php echo lazy_load_class(); ?>" href="<?php echo $about_href; ?>" style="background-image: url(<?php echo lazy_load_src($image['sizes']['large']); ?>);" data-original="<?php echo $image['sizes']['large']; ?>">
		                	</a>
		                </figure>
			        <?php endforeach; ?>
				<?php endif; ?>
			</div>
		</article>
		</div>
	</div>


	<!--The Shop-->
	<?php $test = get_field('add_background_image_shop');  ?>
	<div id="home-shop" class="cf bg-cover<?php echo lazy_load_class(); ?>"<?php if( get_field('add_background_image_shop') ): ?><?php echo ' style="background-image:url('.lazy_load_src(get_field('add_background_image_shop')).');"'; ?><?php endif; ?> data-original="<?php echo $test; ?>">
		<div class="main-wrapper cf">
			<div class="container">
				<div class="first-row visible-animation cf">
					<div class="col-1 item-1">
						<?php
							$shop_id = 25;
							$shop_title = get_the_title($shop_id);
							$shop_href = get_permalink($shop_id);
						?>
						<h3><?php the_field('shop_title') ?></h3>
						<div class="divider center-mobile"></div>
						<div class="shop-desc"><?php the_field('shop_description') ?></div>
					</div>
					<div class="col-1 item-2">
						<?php if( get_field('shop_image_1') ): ?>
							<?php $img_1 = get_field('shop_image_1'); ?>
							<?php echo '<a href="'.$shop_href.'" class="image-1 shop-img scale-img"><img class="lazy-load'.lazy_load_class().'" src="'.lazy_load_src($img_1).'" alt="'.$shop_title.'" data-original="'.$img_1.'"></a>'; ?>
						<?php endif; ?>
					</div>
					<div class="col-2 item-3">
						<?php if( get_field('shop_image_2') ): ?>
							<?php $img_2 = get_field('shop_image_2'); ?>
							<?php echo '<a href="'.$shop_href.'" class="image-2 shop-img scale-img"><img class="lazy-load'.lazy_load_class().'" src="'.lazy_load_src($img_2).'" alt="'.$shop_title.'" data-original="'.$img_2.'"></a>'; ?>
						<?php endif; ?>
					</div>
				</div>
				<div class="second-row visible-animation cf">
					<div class="col-2 item-2">
						<?php if( get_field('shop_image_3') ): ?>
							<?php $img_3 = get_field('shop_image_3'); echo '<a href="'.$shop_href.'" class="image-3 shop-img scale-img half-col left"><img class="lazy-load'.lazy_load_class().'" src="'.lazy_load_src($img_3).'" alt="'.$shop_title.'" data-original="'.$img_3.'"></a>'; ?>
						<?php endif; ?>
						<?php if( get_field('shop_image_4') ): ?>
							<?php $img_4 = get_field('shop_image_4'); echo '<a href="'.$shop_href.'" class="image-4 shop-img scale-img half-col right"><img class="lazy-load'.lazy_load_class().'" src="'.lazy_load_src($img_4).'" alt="'.$shop_title.'" data-original="'.$img_4.'"></a>'; ?>
						<?php endif; ?>
					</div>
					<div class="col-1 item-1">
						 <?php the_field('shop_description_2') ?>
						 <a href="<?php echo $shop_href; ?>" class="btn large black chainsaw-icon"><i>Learn More</i><span class="chainsaw"></span></a>
					</div>
				</div>
			</div>
		</div>
	</div><!--#close-->


</div><!--END #content-->
<?php get_footer(); ?>
