<footer id="footer" class="bg-cover"<?php if( get_field('footer_background', 'option') ): ?><?php echo ' style="background-image:url('.get_field('footer_background', 'option').');"'; ?><?php endif; ?>>
	<div class="inner-wrapper">
		<!--Newsletter form-->
		<div class="newsletter-form cf">
			<?php //if(get_field('embed_form', 'option')) { echo get_field('embed_form', 'option'); } ?>
		</div>


		<!--Footer Menu-->
		<div class="footer-nav cf">
			<nav id="footer-nav" class="cul">
		    	<?php
		    		/*Footer Nav*/
					wp_nav_menu( array(
						'menu' => 'Footer Menu',
						'container'=> ''
					) );
				?>
		    </nav>
		</div>

		<!--Social-->
		<div class="footer-social cf">
			<nav id="f-social" class="cul">
				<ul>
					<?php if(get_field('facebook', 'option')) { echo '<li><a target="_blank" class="fa fa-facebook" title="Facebook" href="' .get_field('facebook', 'option'). '"></a></li>'; } ?>
					<?php if(get_field('instagram', 'option')) { echo '<li><a target="_blank" class="fa fa-instagram" title="Instagram" href="' .get_field('instagram', 'option'). '"></a></li>'; } ?>
				</ul>
			</nav>
		</div>

		<!--Copyrights-->
	    <div class="copyrights">
	    ©<?php echo date("Y"); ?><?php  if(get_field('copyrights', 'option')) { echo get_field('copyrights', 'option'); } ?>
	    </div>
	</div>
</footer>

</div><!--END #page-->

<?php wp_footer(); ?>

<?php if(is_page('media')): ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/isotope.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/media.js"></script>
<?php endif; ?>

<?php
$url = get_site_url();

if($url=='https://www.jaymiron.com' || $url=='http://www.jaymiron.com'):
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93701128-1', 'auto');
  ga('send', 'pageview');
</script>
<?php endif; ?>

<noscript><div class="no-js-msg">You have to enable javascript in your browser settings if you want to view this site</div></noscript>
</body>
</html>
