<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">

		<title><?php
		if (is_home()) {
            echo wp_title('');
        } elseif (is_archive()) {
            echo get_the_archive_title();
        } elseif (is_search()) {
            echo 'Search Results: '.get_search_query();
        } elseif (is_404()) {
            echo '404 - Page Not Found';
        } else {
            echo wp_title('');
        }
		?></title>

		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">


		<?php wp_head(); ?>
</head>
<body <?php body_class();?>>

<!--Svg Animation Preloader-->
<div id="preloader">
	<div class="wrap-loader">
		<div class="svg-logo">
			<?php get_template_part('elements/logo-svg'); ?>
		</div>
		<!--div class="x-logo-pace-mask"></div>
		<div class="x-logo-pace-loader"></div-->
	</div>
</div>

<div id="page" class="page-name-<?php print $post->post_name; ?>" data-name="name.<?php print $post->post_name; ?>">

	<!--Open mobile nav-->
	<div id="mobile-nav-container" class="">
		<a id="close-nav" class="trans200"></a>
		<div class="vcenter-outer">
			<div class="vcenter-inner">
				<div class="wrap-elements">
					<a id="secondary-logo" href="<?php bloginfo('wpurl'); ?>" title="<?php bloginfo('name'); ?>">
					<?php if(get_field('logo','option'))
					{
						echo '<img class="mauto" width="300" src="' . get_field('mobile_logo', 'option') . '" alt="logo">';
					} ?>
					</a>

					<!--Mobile Fullscreen Menu-->
					<nav id="fullscreen-menu" class="cf trans400">
						<?php
							wp_nav_menu( array(
								'menu' => 'Mobile Menu',
								'container'=> ''
							));
						?>
					</nav>

					<!--Social Nav-->
					<nav id="social-nav" class="cul">
						<ul>
							<?php if(get_field('email', 'option')) { echo '<li><a class="fa fa-envelope" title="E-mail" href="mailto:' .get_field('email', 'option'). '"></a></li>'; } ?>
							<?php if(get_field('facebook', 'option')) { echo '<li><a target="_blank" class="fa fa-facebook" title="Facebook" href="' .get_field('facebook', 'option'). '"></a></li>'; } ?>
							<?php if(get_field('instagram', 'option')) { echo '<li><a target="_blank" class="fa fa-instagram" title="Instagram" href="' .get_field('instagram', 'option'). '"></a></li>'; } ?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<!--Copyrights-->
	    <div class="copyrights">
	    ©<?php echo date("Y"); ?><?php if(get_field('copyrights', 'option')) { echo get_field('copyrights', 'option'); } ?>
	    </div>
	</div>

	<header id="header" class="trans200">
		<div class="wrap-header cf">
			<a href="<?php bloginfo('wpurl'); ?>" title="<?php bloginfo('name'); ?>" id="logo" class="trans400">
			<?php if(get_field('logo','option'))
			{
				echo '<img class="trans400" width="280" src="' . get_field('logo', 'option') . '" alt="logo">';
			} ?>
			</a>
			<a href="<?php bloginfo('wpurl'); ?>" title="<?php bloginfo('name'); ?>" id="quote">
			<?php if(get_field('logo_quote','option'))
			{
				echo '<img width="250" src="' . get_field('logo_quote', 'option') . '" alt="quote">';
			} ?>
			</a>
			<nav id="mobile-nav" class="cf trans200">
				<a id="hamburger" class="hamburger right" href="#">
					<span class="line line-1 trans200"></span>
					<span class="line line-2 trans200"></span>
					<span class="line line-3 trans200"></span>
				</a>
			</nav>

			<nav id="main-nav" class="trans200 cf">
				<?php
					wp_nav_menu( array(
						'menu' => 'Main Menu',
						'container'=> ''
					));
				?>
			</nav>
		</div>
	</header>
