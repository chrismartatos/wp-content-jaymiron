<?php
/*
Template Name: Studio - Template
*/
?>
<?php get_header(); ?>

<div id="content" class="subpage subpage-layout header-top">
	<header id="subpage-header" class="header">
		<h1><?php the_title(); ?></h1>
		<div class="divider mauto"></div>
	</header>
	
				
	<?php if( have_posts() ): the_post(); ?>
		
		<?php 
		$imgsrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full");
		$get_featimg = $imgsrc[0]; 
		
		if(!empty($get_featimg)):
		?>
		<div class="container">
			<div class="row">
			<div class="featured-image">
				<img src="<?php echo $get_featimg; ?>" style="max-width:100%;" alt="<?php the_title(); ?>">
			</div>
			</div>
		</div>
		
			<?php //include('elements/banner.php'); ?>
			
		<?php else: ?>
			<div class="no-banner cf">
				
			</div>
		<?php endif; ?>
		
		<div class="container">
			<div class="row">
				
				<article class="entry-content cf">
					<div class="col-lg-12"><?php the_content(); ?></div>
					<?php get_template_part('elements/flexible-content-subpage'); ?>
				</article>
			</div><!--END .row-->
		</div><!--END .container-->
		
		
		<?php get_template_part('elements/featured-work'); ?>
		
	<?php else: ?>
		<p>Sorry, this page not longer exists.</p>
	<?php endif; ?>
	
	
</div><!--END #content-->


<?php get_footer(); ?>
