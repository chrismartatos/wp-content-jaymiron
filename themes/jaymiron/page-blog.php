<?php
/*
Template Name: Blog Template
*/
?>
<?php get_header(); ?>

	<section id="content" itemscope itemtype="http://schema.org/Blog">
		<div class="container">
			<div class="row">
	        <?php query_posts('post_type=post&post_status=publish&posts_per_page=6&paged='. get_query_var('paged')); ?>
	    	<?php if( have_posts() ): ?>
	
	        <?php while( have_posts() ): the_post(); ?>
	
		    <article itemscope itemtype="http://schema.org/blogPost" id="post-<?php get_the_ID(); ?>" <?php post_class('col-md-6 col-sm-6 col-xs-6'); ?>>
		    	<div class="wrap-post">
			    	<a class="trans400 post-thumb" href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(900,900), array('itemprop' => 'image') ); ?></a>
			    	<time class="date" itemprop="dateCreated">
			    		<?php the_date(); ?>
			    	</time>
		        	<h2 itemprop="alternativeHeadline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<div class="description" itemprop="description">
						<?php the_excerpt(__('Continue reading �','example')); ?>
					</div>
					<a class="read-more" itemprop="url" href="<?php the_permalink(); ?>">Read More<span></span></a>
		    	</div>
	        </article><!-- post -->
	        
	        <?php endwhile; ?>
	        </div>
		</div>
	
		<nav class="navigation">
			<span class="newer"><?php previous_posts_link(__('&larr; Newer Articles','example')) ?></span> <span class="older"><?php next_posts_link(__('Previous Articles &rarr;','example')) ?></span>
		</nav><!-- /.navigation -->
	
		<?php else: ?>
	
		<div id="post-404" class="noposts">

		    <p><?php _e('None found.','example'); ?></p>

	    </div><!-- /#post-404 -->

	<?php endif; wp_reset_query(); ?>

	</section><!-- /#content -->

<?php get_footer(); ?>