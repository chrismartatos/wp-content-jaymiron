<?php
/*
Template Name: Media - Template
*/
?>
<?php get_header(); ?>

<div id="content" class="subpage subpage-layout header-top">
	<header id="subpage-header" class="header">
		<h1><?php the_title(); ?></h1>
		<div class="divider mauto"></div>
	</header>


	<?php if( have_posts() ): the_post(); ?>

		<?php
		$imgsrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full");

		if(!empty($imgsrc)):
			$get_featimg = $imgsrc[0];
		?>
		<div class="container">
			<div class="row">
			<div class="featured-image">
				<img src="<?php echo $get_featimg; ?>" style="max-width:100%;" alt="<?php the_title(); ?>">
			</div>
			</div>
		</div>

		<?php else: ?>
			<div class="x-no-banner cf"></div>
		<?php endif; ?>

		<div class="container">
			<div class="row">

				<article class="entry-content cf">
					<div class="col-lg-12"><?php the_content(); ?></div>
				</article>

				<div class="media-wrapper cf">
				<?php
				if( have_rows('add_media') ):

					while ( have_rows('add_media') ) : the_row();
						$title = get_sub_field('title');
						$image_url = get_sub_field('thumbnail');
						$link = get_sub_field('url');
						$cover = get_sub_field('cover');



						$bg_image = (!empty($image_url))? ' style="background-image:url('.$image_url['url'].');"': '';
					?>
					<article class="col-md-4 media-item bg-cover"<?php //echo $bg_image; ?>>
						<div class="wrap-item cf">
						  <?php if($cover=="website"): ?>
						  <a href="<?php echo $link; ?>" title="<?php echo $title; ?>" target="_blank">
						    <img src="<?php echo $image_url['url']; ?>" alt="Jay Miron Media - <?php echo $title; ?>">
						    <h3 class="title"><?php echo $title; ?></h3>
						  </a>
						  <?php else: ?>
						  <a href="<?php echo $link; ?>" title="<?php echo $title; ?>" target="_blank">
						    <img src="<?php echo $image_url['url']; ?>" alt="Jay Miron Media - <?php echo $title; ?>">
						    <h3 class="title"><?php echo $title; ?></h3>
						  </a>
						  <?php endif; ?>
						</div>
					</article>
				<?php
					endwhile;
				endif;
				?>
				</div>
			</div><!--END .row-->
		</div><!--END .container-->


		<?php get_template_part('elements/featured-work'); ?>

	<?php else: ?>
		<p>Sorry, this page not longer exists.</p>
	<?php endif; ?>


</div><!--END #content-->


<?php get_footer(); ?>
