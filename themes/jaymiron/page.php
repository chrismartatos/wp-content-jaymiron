<?php get_header(); ?>

<div id="content" class="subpage subpage-layout">
	<?php if( have_posts() ): the_post(); ?>
		
		<?php 
		$imgsrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full");
		$get_featimg = $imgsrc[0]; 
		
		if(!empty($get_featimg)):
		?>
			<?php include('elements/banner.php'); ?>
			
		<?php else: ?>
			<div class="no-banner cf">
				
			</div>
		<?php endif; ?>
		
		<div class="container">
			<div class="row">
				<header id="subpage-header" class="header">
					<h1><?php the_title(); ?></h1>
					<div class="divider mauto"></div>
				</header>
				<article class="entry-content cf">
					<div class="col-lg-12"><?php the_content(); ?></div>
					<?php get_template_part('elements/flexible-content-subpage'); ?>
				</article>
			</div><!--END .row-->
		</div><!--END .container-->
		
		
		<?php get_template_part('elements/featured-work'); ?>
		
	<?php else: ?>
		<p>Sorry, this page not longer exists.</p>
	<?php endif; ?>
	
	
</div><!--END #content-->


<?php get_footer(); ?>
