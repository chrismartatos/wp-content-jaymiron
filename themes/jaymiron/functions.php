<?php
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

$all_includes = [
	'inc/theme-setup.php',
  'inc/cpt-work.php',
	'inc/acf.php',
];

foreach ($all_includes as $file)
{
  if (!$filepath = locate_template($file))
  {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);



/*-------------------------------------------------------------------------------
	Documentation
-------------------------------------------------------------------------------*/
function theme_doc()
{
	include("admin/admin-shortcodes.php");
}

function addCustomMenuItem()
{
	add_theme_page('Documentation', 'Documentation', 'edit_posts', 'theme-doc', 'theme_doc');
}
add_action('admin_menu', 'addCustomMenuItem');



/*----------------------------------------------------------
		Remove p from shortcodes
----------------------------------------------------------*/
function wpex_clean_shortcodes($content){
$array = array (
    '<p>[' => '[',
    ']</p>' => ']',
    ']<br />' => ']'
);
$content = strtr($content, $array);
return $content;
}
add_filter('the_content', 'wpex_clean_shortcodes');


/*----------------------------------------------------------
		Lazy load FN
----------------------------------------------------------*/

/*Reutn class*/
function lazy_load_class()
{
	if(is_handheld())
	{
		return " lazy-load-init";
	} else {
		return " desktop-device";
	}
}

/*Return Image src*/
function lazy_load_src($image_src)
{
	$loading_gif = get_template_directory_uri().'/images/blank.gif';

	if(is_handheld())
	{
		return $loading_gif;
	} else {
		return $image_src;
	}
}


/*----------------------------------------------------------
		Lazy load
----------------------------------------------------------
function filter_lazyload($content)
{
    return preg_replace_callback('/(<\s*img[^>]+)(src\s*=\s*"[^"]+")([^>]+>)/i', 'preg_lazyload', $content);
}
add_filter('the_content', 'filter_lazyload');


function preg_lazyload($img_match) {

    $img_replace = $img_match[1] . 'src="' . get_stylesheet_directory_uri() . '/images/lazy-load.gif" data-original' . substr($img_match[2], 3) . $img_match[3];

    $img_replace = preg_replace('/class\s*=\s*"/i', 'class="lazy ', $img_replace);

    $img_replace .= '<noscript>' . $img_match[0] . '</noscript>';

    return $img_replace;
}
*/

/*----------------------------------------------------------
		ELEMENTS: Social Media
----------------------------------------------------------*/
function social_media($atts) {

	ob_start();

	include('elements/social-media.php');

	$content = ob_get_clean();

	return $content;
}


add_shortcode('social_media', 'social_media');



/*----------------------------------------------------------
		Customize links
----------------------------------------------------------*/
function add_class_to_image_links($html, $attachment_id, $attachment) {
    $linkptrn = "/<a[^>]*>/";
    $found = preg_match($linkptrn, $html, $a_elem);

    // If no link, do nothing
    if($found <= 0) return $html;

    $a_elem = $a_elem[0];

    // Check to see if the link is to an uploaded image
    $is_attachment_link = strstr($a_elem, "wp-content/uploads/");

    // If link is to external resource, do nothing
    if($is_attachment_link === FALSE) return $html;

    if(strstr($a_elem, "class=\"") !== FALSE){ // If link already has class defined inject it to attribute
        $a_elem_new = str_replace("class=\"", "class=\"img-link", $a_elem);
        $html = str_replace($a_elem, $a_elem_new, $html);
    }else{ // If no class defined, just add class attribute
        $html = str_replace("<a ", "<a  rel=\"image\" class=\"img-link\" ", $html);
    }

    return $html;
}

add_filter('image_send_to_editor', 'add_class_to_image_links', 10, 3);



/*----------------------------------------------------------
		6 col - half
----------------------------------------------------------*/
function my_half_col( $atts, $content = null )
{
	extract( shortcode_atts( array(
		'class' => ''
	), $atts ) );

	return '<div class="col-md-6"><div class="wrap cf '.esc_attr( $class ) . '">' . do_shortcode( $content ) . '</div></div>';

}
add_shortcode('half_column', 'my_half_col');



/*-------------------------------------------------------------------------------
	Custom field for gallery
-------------------------------------------------------------------------------*/
function be_attachment_field_credit( $form_fields, $post ) {
	$form_fields['add-custom-class'] = array(
		'label' => 'Custom Class',
		'input' => 'text',
		'value' => get_post_meta( $post->ID, 'add_custom_class', true ),
		'helps' => 'Add a class if needed',
	);

	return $form_fields;
}
add_filter( 'attachment_fields_to_edit', 'be_attachment_field_credit', 10, 2 );

function be_attachment_field_credit_save( $post, $attachment ) {
	if( isset( $attachment['add-custom-class'] ) )
		update_post_meta( $post['ID'], 'add_custom_class', $attachment['add-custom-class'] );

	return $post;
}
add_filter( 'attachment_fields_to_save', 'be_attachment_field_credit_save', 10, 2 );



/*-------------------------------------------------------------------------------
	Custom Gallery - Photo Galleries
-------------------------------------------------------------------------------*/

function my_custom_gallery($string,$attr)
{
	$type = $attr['theme_gallery_type'];
	$posts = get_posts(array('include' => $attr['ids'],'post_type' => 'attachment', 'orderby' => 'post__in'));
	$output = '';

	if($type == 'slideshow')
	{
		$output .= '<div class="simple-gallery slideshow flexslider"><ul class="slides">';
		foreach($posts as $imagePost)
		{
			$id = $imagePost->post_parent;
			$caption = $imagePost->post_excerpt;
			$img_l = wp_get_attachment_image_src($imagePost->ID, 'full');
			$output .= '<li><a class="item" href="'.$img_l[0].'" title="'.$caption.'" rel="image"><img alt="'.$caption.'" src="'.$img_l[0].'"/></a></li>';
		}
		$output .= '</ul></div>';
	}
	else if($type == 'carousel')
	{
		$output .= '<div class="simple-gallery carousel flexslider"><ul class="slides">';
		foreach($posts as $imagePost)
		{
			$id = $imagePost->post_parent;
			$caption = $imagePost->post_excerpt;
			$img_l = wp_get_attachment_image_src($imagePost->ID, 'full');
			$output .= '<li><a class="item" href="'.$img_l[0].'" title="'.$caption.'" rel="image"><img alt="'.$caption.'" src="'.$img_l[0].'"/></a></li>';
		}
		$output .= '</ul></div>';
	}

	return $output;
}
add_filter('post_gallery','my_custom_gallery',10,2);


function my_galleries()
{
	// define your backbone template;
	// the "tmpl-" prefix is required,
	// and your input field should have a data-setting attribute
	// matching the shortcode name
	?>
	<script type="text/html" id="tmpl-bbt-gallery-setting">
		<label class="setting">
			<span><?php _e('Gallery Type'); ?></span>
			<select data-setting="theme_gallery_type">
				<option value="default">Default Wordpress Gallery</option>
				<option value="slideshow">Simple Slideshow</option>
				<option value="carousel">Carousel Slideshow</option>
			</select>
		</label>
	</script>

	<script>
		jQuery(document).ready(function()
		{
			// add your shortcode attribute and its default value to the
			// gallery settings list; $.extend should work as well...
			_.extend(wp.media.gallery.defaults, {
				theme_gallery_type: 'default'
			});

			// merge default gallery settings template with yours
			wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
				template: function(view)
				{
					return wp.media.template('gallery-settings')(view)
							 + wp.media.template('bbt-gallery-setting')(view);
				}
			});
		});
	</script>
	<?php
}
add_action('print_media_templates', 'my_galleries');
