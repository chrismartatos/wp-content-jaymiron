<?php 
$imgsrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full");
$get_featimg = $imgsrc[0]; 


$parallax = 'data-top-bottom="background-position: 50% -120px;" data-bottom-top="background-position: 50% 120px;"';
?>
<section id="hero" class="cf full-height bg-cover bg-fixed" <?php if(!is_page('about')){ echo $parallax; } ?> <?php echo 'style="background-image:url('.$get_featimg.')"';?>>
	
	<?php if( get_field('banner_quote') ): ?>
	<div class="quote">
		<h2><?php the_field('banner_quote'); ?></h2>
	</div>
	<?php endif; ?>
	
	<a class="scroll-to wrap-scroll" href="#subpage-header" data-offset="20">
		<div class="text"><?php the_field('keep_scrolling_text'); ?></div>
		<span class="scroll-arrows one"></span>
		<span class="scroll-arrows two"></span>
		<span class="scroll-arrows three"></span>
	</a>		
</section>