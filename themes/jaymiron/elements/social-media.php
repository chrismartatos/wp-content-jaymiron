<nav id="social-pages" class="cul">
	<ul>
		<?php if(get_field('facebook', 'option')) { echo '<li><a target="_blank" class="fa fa-facebook" title="Facebook" href="' .get_field('facebook', 'option'). '"></a></li>'; } ?>
		<?php if(get_field('instagram', 'option')) { echo '<li><a target="_blank" class="fa fa-instagram" title="Instagram" href="' .get_field('instagram', 'option'). '"></a></li>'; } ?>
	</ul>
</nav>