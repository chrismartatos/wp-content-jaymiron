<?php

//Check	
if( have_rows('flexible_content_1') ):

 // loop through
while ( have_rows('flexible_content_1') ) : the_row();
	
	
	
	/*--------------------------------------------------------
		Text: Fullwidth 
	--------------------------------------------------------*/
    if( get_row_layout() == 'text_fullwidth' ):	
	
	?>
	<div class="flexible-content-one visible-animation fc-fullwidth-text <?php if(get_sub_field('centered_text') == "Centered") { echo " centered-header"; } ?>">
    <div class="cf text-wrapper">
		<article class="cf <?php if(get_sub_field('centered_text') == "Centered") { echo " centered-wrapper"; } ?>">
		<?php
    		the_sub_field('fullwidth'); 
    	?>
		</article>
	</div>
	</div>
	<?php
	
	
	
	/*--------------------------------------------------------
		 Text: Two columns 
	--------------------------------------------------------*/
	elseif( get_row_layout() == 'text_two_columns' ):

	?>
	<div class="flexible-content-one visible-animation fc-text-two-col">
    <div class="row">
	    <div class="cf text-wrapper">
        <article class="col-lg-6 col-md-6 col-sm-6"><?php the_sub_field('content_left'); ?></article>
        <article class="col-lg-6 col-md-6 col-sm-6"><?php the_sub_field('content_right'); ?></article>
	    </div>
	</div>
	</div>
	<?php



	/*--------------------------------------------------------
		 Text: Two columns 
	--------------------------------------------------------*/
	elseif( get_row_layout() == 'image_fullwidth' ):
	
	$image = get_sub_field('fullwidth_image');
	?>
	<div class="flexible-content-one visible-animation gallery-scale touch-event fc-fullwidth-image">
    <div class="row">
        <article class="col-lg-12 ">
	        <figure>
	        	<img class="lazy-load<?php echo lazy_load_class(); ?>" src="<?php echo lazy_load_src($image['url']); ?>" alt="<?php echo $image['alt']; ?>" data-original="<?php echo $image['url']; ?>">
	        </figure>
	    </article>
	</div>
	</div>
	<?php
	
	
	
	/*--------------------------------------------------------
		Text: two columns - 30% left
	--------------------------------------------------------*/
	elseif( get_row_layout() == 'text_two_columns_left_30' ):
	?>
	<div class="flexible-content-one visible-animation fc-text-left-col">
    <div class="row">
	    <div class="cf text-wrapper">
        <article class="col-lg-4 col-md-4 col-sm-4"><?php the_sub_field('content_left'); ?></article>
        <article class="col-lg-8 col-md-8 col-sm-8"><?php the_sub_field('content_right'); ?></article>
	    </div>
	</div>
	</div>
	<?php
    
    
    
    /*--------------------------------------------------------
		Text: two columns - 30% Right
	--------------------------------------------------------*/
	elseif( get_row_layout() == 'text_two_columns_right_30' ):

	?>
	<div class="flexible-content-one visible-animation fc-text-right-col">
    <div class="row">
	    <div class="cf text-wrapper">
        <article class="col-lg-8 col-md-8 col-sm-8"><?php the_sub_field('content_left'); ?></article>
        <article class="col-lg-4 col-md-4 col-sm-4"><?php the_sub_field('content_right'); ?></article>
	    </div>
	</div>
	</div>
	<?php
    
	
    /*--------------------------------------------------------
		Text: Three columns
	--------------------------------------------------------*/
	elseif( get_row_layout() == 'text_three_columns' ):

	?>
	<div class="flexible-content-one visible-animation fc-text-three-col">
    <div class="row">
	    <div class="cf text-wrapper">
        <article class="col-lg-4 col-md-4 col-sm-4"><?php the_sub_field('content_1'); ?></article>
        <article class="col-lg-4 col-md-4 col-sm-4"><?php the_sub_field('content_2'); ?></article>
        <article class="col-lg-4 col-md-4 col-sm-4"><?php the_sub_field('content_3'); ?></article>
	    </div>
	</div>
	</div>
	<?php
		
	/*--------------------------------------------------------
		Text: Four columns
	--------------------------------------------------------*/
	elseif( get_row_layout() == 'text_four_columns' ):

	?>
	<div class="flexible-content-one visible-animation fc-text-four-col">
    <div class="row">
	    <div class="cf text-wrapper">
        <article class="col-lg-3 col-md-3 col-sm-3"><?php the_sub_field('content_1'); ?></article>
        <article class="col-lg-3 col-md-3 col-sm-3"><?php the_sub_field('content_2'); ?></article>
        <article class="col-lg-3 col-md-3 col-sm-3"><?php the_sub_field('content_3'); ?></article>
        <article class="col-lg-3 col-md-3 col-sm-3"><?php the_sub_field('content_4'); ?></article>
	    </div>
	</div>
	</div>
	<?php

	
	/*--------------------------------------------------------
		Images: 4 images row
	--------------------------------------------------------*/
	elseif( get_row_layout() == 'images_four_row' ):
	
	$rows = get_sub_field('gallery');
	
	
    if ($rows) : 
	?>
	<div class="flexible-content-one visible-animation gallery-scale touch-event">
	<div class="row">
        <?php foreach ($rows as $row) : $img = wp_get_attachment_image_src($row['ID'], 'large'); ?>
           <figure class="col col-lg-3 col-md-3 col-sm-3">
           		<div class="bg-cover" data-rel="image" data-url="<?php echo $img[0]; ?>">
               		<img class="lazy-load<?php echo lazy_load_class(); ?>" src="<?php echo lazy_load_src($img[0]); ?>" alt="<?php the_title(); ?>" data-original="<?php echo $image[0]; ?>">
           		</div>
            </figure>
		<?php endforeach; ?>
	</div>
	</div>
	<?php
  	endif;  //End $rows
	
	
						
	/*--------------------------------------------------------
		Images: Left portrait 2 landscape
	--------------------------------------------------------*/
	elseif( get_row_layout() == 'grid_system_for_images_1' ):
	
	$rows = get_sub_field('gallery');
	$class = "";
	
    if ($rows) : 
	?>
	<div class="flexible-content-one visible-animation gallery-scale touch-event">
	<div class="row">
		<article class="cf gallery-styles gallery-style-3 col-lg-12">
        <?php foreach ($rows as $row) : 
	        $img = wp_get_attachment_image_src($row['ID'], 'large'); 
	        $class = get_post_meta( $row['ID'], 'add_custom_class', true ); ?>
           <figure class="col left <?php echo $class; ?>">
           		<div class="bg-cover img-link lazy-load<?php echo lazy_load_class(); ?>" data-original="<?php echo $img[0]; ?>" style="background-image:url(<?php echo lazy_load_src($img[0]); ?>);"></div>
            </figure>
		<?php endforeach; ?>
        </article>
	</div>
	</div>
	<?php
  	endif;  //End $rows
  	
  	/*--------------------------------------------------------
		Images: Right portrait 2 landscape
	--------------------------------------------------------*/
	elseif( get_row_layout() == 'grid_system_for_images_2' ):
	
	$rows = get_sub_field('gallery');
	$class = "";
	
    if ($rows) : 
	?>
	<div class="flexible-content-one visible-animation gallery-scale touch-event">
	<div class="row">
		<article class="cf gallery-styles gallery-style-right-3 col-lg-12">
        <?php foreach ($rows as $row) : 
	        $img = wp_get_attachment_image_src($row['ID'], 'large'); 
	        $class = get_post_meta( $row['ID'], 'add_custom_class', true ); ?>
           <figure class="col left <?php echo $class; ?>">
           		<div class="bg-cover img-link lazy-load<?php echo lazy_load_class(); ?>" data-original="<?php echo $img[0]; ?>" style="background-image:url(<?php echo lazy_load_src($img[0]); ?>);"></div>
            </figure>
		<?php endforeach; ?>
        </article>
	</div>
	</div>
	<?php
  	endif;  //End $rows
  	
  	
  	/*--------------------------------------------------------
		Images: Equal colums
	--------------------------------------------------------*/
	elseif( get_row_layout() == 'equal_left_and_right' ):
	
	$rows = get_sub_field('gallery');
	$class = "";
	
    if ($rows) : 
	?>
	<div class="flexible-content-one visible-animation gallery-scale touch-event">
	<div class="row">
		<article class="cf gallery-styles gallery-style-2">
        <?php foreach ($rows as $row) : 
	        $img = wp_get_attachment_image_src($row['ID'], 'large'); 
	        $class = get_post_meta( $row['ID'], 'add_custom_class', true ); ?>
           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
	           <figure>
           		<div class="bg-cover img-link lazy-load<?php echo lazy_load_class(); ?> <?php echo $class; ?>" data-original="<?php echo $img[0]; ?>" data-title="<?php echo $row['alt']; ?>" style="background-image:url(<?php echo lazy_load_src($img[0]); ?>);"></div>
	           </figure>
            </div>
		<?php endforeach; ?>
        </article>
	</div>
	</div>
	<?php
  	endif;  //End $rows
  	
  	
  	
  	/*--------------------------------------------------------
		Images: Equal colums
	--------------------------------------------------------*/
	elseif( get_row_layout() == 'equal_left_and_right_full' ):
	
	$rows = get_sub_field('gallery');

    if ($rows) : 
	?>
	<div class="flexible-content-one visible-animation gallery-scale touch-event">
	<div class="row">
		<article class="cf gallery-style-2-full">
        <?php foreach ($rows as $row) : $img = wp_get_attachment_image_src($row['ID'], 'large'); ?>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	           <figure>
	           <img class="lazy-load<?php echo lazy_load_class(); ?>" src="<?php echo lazy_load_src($img[0]); ?>" alt="<?php the_title(); ?>" data-original="<?php echo $img[0]; ?>">
	           </figure>
	        </div>
		<?php endforeach; ?>
        </article>
	</div>
	</div>
	<?php
  	endif;  //End $rows
  	
  	
      	
    endif;//END row layout

endwhile; //End loop through

endif; //End check
?>