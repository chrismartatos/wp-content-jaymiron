<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
<article class="project-item touch-event bg-cover visible-animation" style="background-image: url(<?php echo $feat_image; ?>);">
	<div class="wrap-info">
		<div class="vcenter-outer">
			<div class="vcenter-inner">
				<div class="inner trans400">
					<h4><?php the_title(); ?></h4>
					<p><?php the_field('small_description') ?></p>
					<a href="<?php the_permalink(); ?>" class="btn small white">VIEW PROJECT</a>
				</div>
			</div>
		</div>
	</div>
</article>