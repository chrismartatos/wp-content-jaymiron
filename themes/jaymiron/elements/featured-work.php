<div id="featured-work" class="cf">
	<header class="wrap-head">
		<?php 
			$work_page = 21;
			$work_title = get_field("featured_work",$work_page); 
			$work_desc = get_field("featured_work_description",$work_page); 
		?>
		<h5><?php echo $work_title; ?></h5>
		<span class="divider mauto"></span>
		<?php echo $work_desc; ?>
	</header>
	<div class="wrap-projects cf">	
	<?php
	//Show category 1 featured posts
	$args = array( 
		'post_type' => 'work', 
		'posts_per_page'=> '3',
		'order'=> 'DESC',
		'cat'=> '1',
		'orderby' => 'date',
		'post_status' => 'publish'
	);
	
	//Define the loop based on arguments
	$query = new WP_Query( $args );
	
	//Display the projects
	while ( $query->have_posts() ) : $query->the_post();		
	?>
	
	<?php include('project-item.php'); ?>
	
	<?php 
		endwhile;
		wp_reset_postdata();
	?>
	</div>
</div>