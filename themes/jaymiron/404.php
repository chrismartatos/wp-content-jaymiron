<?php get_header(); ?>

<div id="content" class="cf relative suppage-template">
	<section class="cf tc">
		<h1>404 ERROR</h1>
		<div class="wrap-content cf">
			<div class="container">
			<div class="row">
				
				<article class="entry-content cf">
				
				<div class="tc">
				<p>The page you are looking for does not exist. Use the following links to navigate through the site:</p>
				</div>
					<nav class="tc pt20 pb30 cul">
						<ul style="margin:0; padding: 0;">
							<li><a href="<?php bloginfo('wpurl'); ?>">HOME</a></li>
							<li><a href="/about/">ABOUT</a></li>
							<li><a href="/work/">WORK</a></li>
							<li><a href="/contact/">CONTACT</a></li>
						</ul>
					</nav>
				</article>
			</div>
			</div>
		</div>
		
	</section><!-- content -->
</div>
<?php get_footer(); ?>
