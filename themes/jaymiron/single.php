<?php
defined( 'ABSPATH' ) || exit;

?>

<?php get_header(); ?>

<section id="content">
	<div class="container">
		<?php if( have_posts() ): the_post(); ?>
		<h1 class="main-title"><?php echo get_the_title($ID); ?></h1>
		<time class="date"><?php the_date(); ?></time>

		<article class="entry-content">
			<?php the_content(); ?>
		</article>

		<?php else: ?>
			<p>Sorry, this page not longer exists.</p>
		<?php endif; ?>
	</div>
</section>
<?php get_footer(); ?>
