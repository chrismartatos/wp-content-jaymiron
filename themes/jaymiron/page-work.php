<?php
/*
Template Name: Work - Template
*/
?>
<?php get_header(); ?>

<section id="content" class="subpage subpage-layout work-template">
	<?php if( have_posts() ): the_post(); ?>		
		<?php 
		$imgsrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full");
		$get_featimg = $imgsrc[0]; 
		
		if(!empty($get_featimg)):
		?>
			<?php include('elements/banner.php'); ?>
			
		<?php else: ?>
			<div class="no-banner cf">
				
			</div>
		<?php endif; ?>
		
		<div class="container">
			<div class="row">
				<header id="subpage-header" class="header">
					<h1><?php the_title(); ?></h1>
					<div class="divider mauto"></div>
				</header>
				<article class="entry-content cf">
					<?php the_content(); ?>
					<?php include('elements/flexible-content-subpage.php'); ?>
				</article>
			</div><!--END .row-->
		</div><!--END .container-->
		
		<?php else: ?>
		<p>Sorry, this page not longer exists.</p>
		<?php endif; ?>
		
		<div id="work-projects" class="cf">
			<div class="wrap-projects cf">
			<?php
			//Show all posts 
			$args = array( 
				'post_type' => 'work', 
				'posts_per_page'=> -1,
				'order'=>'DESC',
				'orderby' => 'date',
				'post_status' => 'publish'
			);
			
			//Define the loop based on arguments
			$query = new WP_Query( $args );
			
			//Display the projects
			while ( $query->have_posts() ) : $query->the_post();
			
			//Get image source
			$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			?>
			
			<?php include('elements/project-item.php'); ?>
			
			<?php 
				endwhile;
				wp_reset_postdata();
			?>
			
			</div>
		</div>		
		
</section>

<?php get_footer(); ?>